﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SentinelController : MonoBehaviour {

    public Camera sentinelCamera;
    public Light sentinelSpot;
    public GameObject Head;

    private int rayCount = 10;
    public int searchAngle = 10;
    public float speed = 1;

    private int sentinelAngle = 0;

	void Start ()
    {
		
	}
	
	void Update ()
    {
        sentinelSpot.spotAngle = searchAngle;
        sentinelCamera.fieldOfView = searchAngle;
        rayCount = searchAngle;
        
        for (int i = 0; i <= rayCount; i++)
        {
            var ray = sentinelCamera.ViewportPointToRay(new Vector3(0.5f, 1.0f / rayCount * i, 0));
            Debug.DrawRay(ray.origin, ray.direction * 100f, Color.magenta);
        }

        sentinelAngle = (int)((Time.time * speed) % 360);
        var searchLightAngle = Mathf.Rad2Deg * Mathf.Sin((Time.time * speed*0.1f));
        Head.transform.localRotation = Quaternion.Euler(searchLightAngle, 0, 0);
        this.gameObject.transform.rotation = Quaternion.Euler(0, sentinelAngle, 0);
	}
}

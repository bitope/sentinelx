﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject Head;

    public GameObject marker;

    public Material red;
    public Material green;

    public GameObject currentMarkedObject;
    public Vector3 currentMarkedTile;

    private bool vrEnabled = false;
    private Item me;

    private LayerMask levelLayerMask;
    private void Start()
    {
        levelLayerMask = LayerMask.NameToLayer("Level");
        me = GetComponent<Item>();
        marker = GameObject.Find("Sphere");

        if (UnityEngine.XR.XRDevice.isPresent)
        {
            Debug.Log(UnityEngine.XR.XRDevice.model+ " found.");
            vrEnabled = true;
            Camera.main.transform.SetParent(this.transform);
            Head.transform.SetParent(Camera.main.transform);
        }

        foreach (var s in UnityEngine.Input.GetJoystickNames())
        {
            // "OpenVR Controller - Right"
            // "OpenVR Controller - Left"
            Debug.Log(s);
        }

    }

    private void Update()
    {
        currentMarkedObject = null;
        currentMarkedTile = Vector3.zero;

        Ray ray = new Ray(Head.transform.position, Head.transform.forward);
        RaycastHit info;
        Debug.DrawRay(ray.origin, ray.direction, Color.yellow);
        marker.transform.position = Vector3.zero;

        int x = 0, z = 0;
        if (Physics.Raycast(ray, out info, float.MaxValue, (1 << levelLayerMask.value))) //Only raycast against the Level-layer
        {
            x = (int)info.point.x;
            z = (int)info.point.z;
            if (info.collider.gameObject.name == "Level")
            {
                if (LevelManager.Instance.level.isPlayable[x, z])
                {
                    marker.transform.position = new Vector3(x + 0.5f, info.point.y, z + 0.5f);
                    marker.GetComponent<Renderer>().material = green;
                    Debug.DrawRay(ray.origin, ray.direction, Color.green, 0.1f, true);
                    currentMarkedTile = marker.transform.position - LevelManager.offset;
                }
            }
        }

        if (Physics.Raycast(ray, out info))
        {
            if (info.collider.gameObject.name != "Level")
            {
                currentMarkedObject = info.collider.gameObject;
                marker.GetComponent<Renderer>().material = red;
            }
        }

        if (vrEnabled)
        {
            HandleVRInput();
        }
        else
        {
            HandleInput();
        }
    }

    private void HandleVRInput()
    {
        var v = UnityEngine.Input.GetAxis("Vive Right Trigger");
        var f = UnityEngine.Input.GetAxis("Vive Right TriggerF");
        var x = UnityEngine.Input.GetAxis("Vive Right TrackX");
        var y = UnityEngine.Input.GetAxis("Vive Right TrackY");
        var tp = UnityEngine.Input.GetAxis("Vive Right TouchPress");
        var g = UnityEngine.Input.GetAxis("Vive Right Grip");
        var aa = Angle(new Vector2(x, y));
        Debug.Log(aa+" , "+f*100+" , "+new Vector3(x,y,tp)+" , "+g);
    }

    public static float Angle(Vector2 p_vector2)
    {
        if (p_vector2.x < 0)
        {
            return 360 - (Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg * -1);
        }
        else
        {
            return Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg;
        }
    }

    private void HandleInput()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            LevelManager.Instance.ToggleCursor();
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            if (currentMarkedObject != null && currentMarkedObject.name == "Boulder")
            {
                LevelManager.Instance.AbsorbObject(currentMarkedObject, me);
            } else 
            if (currentMarkedTile != Vector3.zero)
            {
                LevelManager.Instance.AbsorbObject((int)currentMarkedTile.x, (int)currentMarkedTile.z,me);
            }
        }

        if (Input.GetKeyUp(KeyCode.B))
        {
            if (currentMarkedTile != Vector3.zero)
            {
                LevelManager.Instance.CreateBoulder((int)currentMarkedTile.x, (int)currentMarkedTile.z, me);
            }
        }

        if (Input.GetKeyUp(KeyCode.S))
        {
            if (currentMarkedTile != Vector3.zero)
            {
                LevelManager.Instance.CreateSynthoidShell((int)currentMarkedTile.x, (int)currentMarkedTile.z, me);
            }
        }

        if (Input.GetKeyUp(KeyCode.T))
        {
            if (currentMarkedObject != null && currentMarkedObject.name == "Synthoid")
            {
                Debug.Log("Transfer");
                LevelManager.Instance.TransferToSynthoid(currentMarkedObject);
                currentMarkedObject = null;
            }

            if (currentMarkedTile != Vector3.zero)
            {
                
            }
        }


    }
}

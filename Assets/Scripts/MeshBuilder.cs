﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class MeshBuilder : MonoBehaviour
{
    public int seed;
    public int sizeX, sizeZ;

    private Vector3[] vertices;
    private Mesh mesh;

    private int [,] levelheights;
    public bool[,] isPlayable;
    public Tile[,] tiles;

    public Texture2D levelTexture;

    private void Awake()
    {
        if (sizeX % 2 == 1)
            sizeX++;

        if (sizeZ % 2 == 1)
            sizeZ++;

        GenerateLevelHeights(seed);
        Generate();
        //CreateQuads();
        var mc = gameObject.AddComponent<MeshCollider>();
        mc.sharedMesh = mesh;
        GenerateTexture();
    }

    private void GenerateTexture()
    {
        levelTexture = new Texture2D(sizeX, sizeZ);
        tiles = new Tile[sizeX, sizeZ];
        for (int z = 0; z < sizeZ; z++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                var dark = (x + z) % 2 == 0;
                if (!isPlayable[x, z])
                {
                    if (dark)
                        levelTexture.SetPixel(x, z, new Color(0.3f, 0.3f, 0.3f));
                    else
                        levelTexture.SetPixel(x, z, Color.grey);
                }
                else
                {
                    if (dark)
                        levelTexture.SetPixel(x, z, new Color(0, 0.5f, 0));
                    else
                        levelTexture.SetPixel(x, z, Color.green);
                }

                tiles[x, z] = new Tile(new Vector3(x, GetHeight(x, z), z));
            }
        }
        levelTexture.filterMode = FilterMode.Point;
        levelTexture.Apply();
        GetComponent<Renderer>().material.mainTexture = levelTexture;
    }

    private void CreateQuads()
    {
        for (int z = 0; z < sizeZ; z++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                if (isPlayable[x, z])
                {
                    var q = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    q.transform.SetParent(this.transform);
                    q.transform.localPosition = new Vector3(x+0.5f, GetHeight(x, z)+0.01f, z+0.5f);
                    q.transform.rotation = Quaternion.Euler(90, 0, 0);
                    
                }
            }
        }

    }

    private void GenerateLevelHeights(int seed)
    {
        levelheights = new int[sizeX / 2 + 1, sizeZ / 2 + 1];
        isPlayable = new bool[sizeX, sizeZ];

        Random.InitState(seed);

        for (int z = 0; z < sizeZ / 2; z++)
        {
            for (int x = 0; x < sizeX / 2; x++)
            {
                levelheights[x, z] = (int)NormalizedRandom2(0, 5);//Random.Range(0, 3);
            }
        }
    }

    public float GetHeight(int x, int y)
    {
        return (int)levelheights[x / 2, y / 2];
    }

    private float NormalizedRandom(int min, int max)
    {
        var mean = (min + max) / 2.0f;
        var sigma = (max - mean) / 3.0f;
        return Random.Range(mean, sigma);
    }

    private float NormalizedRandom2(float min, float max)
    {
        float mean = 0;
        float stdDev = 1;
        float u1 = Random.value; 
        float u2 = Random.value; 
        float randStdNormal = Mathf.Sqrt(-2.0f * Mathf.Log(u1)) * Mathf.Sin(2.0f * Mathf.PI * u2);
        randStdNormal = (randStdNormal / 3.0f) * max + min;
        return (mean + stdDev * randStdNormal);
    }

    private void Generate()
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Procedural Grid";

        vertices = new Vector3[(sizeX + 1) * (sizeZ + 1)];
        Vector2[] uv = new Vector2[vertices.Length];

        for (int i = 0, z = 0; z <= sizeZ; z++)
        {
            for (int x = 0; x <= sizeX; x++, i++)
            {
                vertices[i] = new Vector3(x, GetHeight(x,z), z);
                if (x==0 || x==sizeX)
                    vertices[i] = new Vector3(x, -10, z);
                if (z == 0 || z == sizeZ)
                    vertices[i] = new Vector3(x, -10, z);

                uv[i] = new Vector2(x / (float)sizeX, z / (float)sizeZ);
            }
        }
        mesh.vertices = vertices;
        mesh.uv = uv;

        int[] triangles = new int[sizeX * sizeZ * 6];
        for (int ti = 0, vi = 0, y = 0; y < sizeZ; y++, vi++)
        {
            for (int x = 0; x < sizeX; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + sizeX + 1;
                triangles[ti + 5] = vi + sizeX + 2;

                // check if this quad is level (and thus playable)
                var p1 = vertices[triangles[ti]];
                var p2 = vertices[triangles[ti+3]];
                var p3 = vertices[triangles[ti+4]];
                var p4 = vertices[triangles[ti+5]];
                if ((int) p1.y == (int)p2.y && (int)p1.y== (int)p3.y && (int)p1.y== (int)p4.y)
                {
                    isPlayable[x, y] = true;
                }
            }
        }

        mesh.triangles = triangles;


        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }

 

    public IEnumerable<Tile> PlayableTiles()
    {
        for (int x = 0; x < sizeX ; x++)
        {
            for (int y = 0; y < sizeZ ; y++)
            {
                if (isPlayable[x, y])
                    yield return tiles[x, y];
                    //yield return new Vector3(x, GetHeight(x, y), y);
            }
        }
        yield break;
    }

    //private void OnDrawGizmos()
    //{
    //    if (vertices == null)
    //    {
    //        return;
    //    }
    //    for (int i = 0; i < vertices.Length; i++)
    //    {
    //        Gizmos.color = Color.black;
    //        Gizmos.DrawSphere(vertices[i], 0.1f);
    //        //Gizmos.color = Color.yellow;
    //        //Gizmos.DrawRay(vertices[i], mesh.normals[i]);
    //    }
    //    Gizmos.color = Color.yellow;
    //    for (int i = 0; i < mesh.triangles.Length; i+=3)
    //    {
    //        var p1 = vertices[mesh.triangles[i]];
    //        var p2 = vertices[mesh.triangles[i+1]];
    //        var p3 = vertices[mesh.triangles[i+2]];

    //        var n1 = mesh.normals[mesh.triangles[i]];
    //        var n2 = mesh.normals[mesh.triangles[i + 1]];
    //        var n3 = mesh.normals[mesh.triangles[i + 2]];

    //        var center = ((p1 + p2 + p3) / 3);
    //        var normal = ((n1 + n2 + n3) / 3);

    //        Gizmos.DrawRay(center, normal);
    //    }
    //}
}


﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Tile
{
    public Vector3 position;
    public List<Item> items;

    public Tile(Vector3 _position)
    {
        position = _position;
        items = new List<Item>();
    }

    public void Reorganize()
    {
        var hh = position.y;
        foreach (var item in items)
        {
            item.MoveToPosition(new Vector3(item.transform.position.x, hh, item.transform.position.z));
            hh += 1;
        }
    }

    public int DestroyLowest()
    {
        if (items.Count > 0)
        {
            var lowest = items.OrderBy(i => i.transform.position.y).First();
            var energy = lowest.energy;
            items.Remove(lowest);
            Debug.Log("DestroyLowest: " + lowest.name + "(" + lowest.transform.position + ")");
            lowest.Absorb();
            Reorganize();
            return energy;
        }
        return 0;
    }
}


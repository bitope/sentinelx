﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    None,
    Tree,
    Boulder,
    Meanie,
    Sentry,
    Synthoid,
    Sentinel
}

public class Item : MonoBehaviour 
{
    public int energy;
    public ItemType type;

    public Vector3 targetPosition;
    public int currentEnery;
    public Tile tile;

    public void MoveToPosition(Vector3 target)
    {
        StartCoroutine(MoveTo(target));
    }

    private IEnumerator MoveTo(Vector3 target)
    {
        var startpos = this.transform.position;
        targetPosition = target;
        var i = 0.0f;
        var rate = 1.0f / 0.5f;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            this.transform.position = Vector3.Lerp(startpos, targetPosition, i);
            yield return null;
        }
    }

    public void Absorb()
    {
        tile = null;
        Destroy(this.gameObject);
    }
}

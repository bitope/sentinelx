﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelManager : MonoBehaviour
{
    public static readonly Vector3 offset = new Vector3(0.5f, 0, 0.5f);
    private static LevelManager _levelManager;
    public static LevelManager Instance { get { return _levelManager; } }

    [Header("Prefabs")]
    public GameObject playerPrefab;
    public GameObject synthoidPrefab;
    public GameObject sentinelPrefab;
    public GameObject sentryPrefab;
    public GameObject boulderPrefab;
    public GameObject treePrefab;
    public GameObject meaniePrefab;

    [HideInInspector]
    public MeshBuilder level;

    [Header("Game values")]
    public bool showCursor;
    public int currentWorldEnergy;
    public int currentPlayerEnergy;
    public GameObject player;

    private void Awake()
    {
        if (_levelManager!=null)
        {
            Debug.Log("Multiple Levelmanagers, destroying this one.");
            DestroyImmediate(this.gameObject);
        }

        _levelManager = this;
    }

    private void Start()
    {
        //var pt = new List<Vector3>();
        //foreach (var tile in level.PlayableTiles())
        //{
        //    pt.Add(tile);
        //}

        PopulateLevel();
    }

    public void PopulateLevel()
    {
        var tiles = level.PlayableTiles().OrderByDescending(i => i.position.y).ToList();
        GenerateSentinel(tiles);
        GeneratePlayer(tiles);
        GenerateTrees(tiles);
    }

    private void GenerateTrees(List<Tile> tiles)
    {
        var trees = tiles.Count / 7;
        while (trees > 0)
        {
            var tile = tiles[Random.Range(tiles.Count / 2, tiles.Count)];
            var treeTile = tile.position;
            tiles.Remove(tile);
            AddItemToLevel(tile.position.x, tile.position.z, treePrefab);
            trees--;
        }
    }

    private void GeneratePlayer(List<Tile> tiles)
    {
        var tile = tiles[Random.Range(tiles.Count / 2, tiles.Count)];
        var playerTile = tile.position;
        tiles.Remove(tile);
        player = AddItemToLevel(tile.position.x, tile.position.z, playerPrefab);
    }

    private void GenerateSentinel(List<Tile> tiles)
    {
        var tile = tiles.First();
        var sentineltile = tile.position;
        tiles.Remove(tile);
        AddItemToLevel(tile.position.x, tile.position.z, sentinelPrefab);
    }


    //--------------------


    internal void ToggleCursor()
    {
        throw new NotImplementedException();
    }

    public void CreateTree(int x, int z)
    {
    }

    public void CreateBoulder(int x, int z, Item who)
    {
        if (CanPlaceBoulderAt(x, z))
        {
            var e = boulderPrefab.GetComponent<Item>().energy;
            if (who.currentEnery >= e)
            {
                who.currentEnery -= e;
                AddItemToLevel(x, z, boulderPrefab);
            }
        }
    }

    private bool CanPlaceBoulderAt(int x, int z)
    {
        var highest = level.tiles[x, z].items.OrderByDescending(i => i.transform.position.y).FirstOrDefault();
        if (highest!=null && highest.name != "Boulder")
        {
            Debug.Log("GameRule: Can Only stack boulders on empty and other boulders.");
            return false;
        }
        return true;
    }

    public void CreateBoulder(GameObject boulder)
    {
    }

    public void AbsorbBoulder(GameObject bottomBoulder)
    {
    }

    public void CreateSynthoidShell(int x, int z, Item who)
    {
        if (CanPlaceShellAt(x, z))
        {
            var e = synthoidPrefab.GetComponent<Item>().energy;
            if (who.currentEnery >= e)
            {
                who.currentEnery -= e;
                AddItemToLevel(x, z, synthoidPrefab);
            }
        }
    }

    private bool CanPlaceShellAt(int x, int z)
    {
        var highest = level.tiles[x, z].items.OrderByDescending(i => i.transform.position.y).FirstOrDefault();
        if (highest != null && highest.name != "Boulder")
        {
            Debug.Log("GameRule: Can Only stack shells on empty and boulders.");
            return false;
        }
        return true;
    }

    public void CreateSynthoidShell(GameObject boulder)
    {
    }

    public void TransferToSynthoid(GameObject synthoid)
    {
        //find my square
        var playerItem = player.GetComponent<Item>();
        var shellItem = synthoid.GetComponent<Item>();
        playerItem.tile.items.Remove(playerItem);
        var pTile = playerItem.tile;
        shellItem.tile.items.Remove(shellItem);
        var sTile = shellItem.tile;
        playerItem.tile = sTile;
        shellItem.tile = pTile;
        shellItem.tile.items.Add(shellItem);
        playerItem.tile.items.Add(playerItem);
        var tempPosition = playerItem.transform.position;
        var tempRotation = playerItem.transform.rotation;
        playerItem.transform.position = shellItem.transform.position;
        playerItem.transform.rotation = shellItem.transform.rotation;
        shellItem.transform.position = tempPosition;
        shellItem.transform.rotation = tempRotation;
        //find synthoid square
        //Swap
        //Swap(synthoid, player);
    }

    public void Hyperspace()
    {
        // waste 3 energy
        // teleport to random playable tile that is not higher than players current position.
    }

    public IEnumerable<Item> FindItemsInLevel(int x, int z)
    {
        foreach (var item in level.tiles[x, z].items)
        {
            yield return item;
        }
    }

    public GameObject AddItemToLevel(float x, float z, GameObject item)
    {
        return AddItemToLevel((int)x, (int)z, item);
    }

    public GameObject AddItemToLevel(int x, int z, GameObject itemPrefab)
    {
        var go = Instantiate(itemPrefab, level.transform);
        go.name = itemPrefab.name;
        var item = go.GetComponent<Item>();
        item.tile = level.tiles[x, z];
        level.tiles[x, z].items.Add(item);
        item.transform.position = level.tiles[x, z].position + offset+new Vector3(0, level.tiles[x, z].items.Count-1,0);
        item.transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.up);
        currentWorldEnergy += item.energy;
        return go;
    }

    public void AbsorbObject(GameObject gameObject, Item who)
    {
        var item = gameObject.GetComponent<Item>();
        var lowest = item.tile.items.OrderBy(i => i.transform.position.y).First();
        if (item == lowest)
        {
            var energy = item.tile.DestroyLowest();
            who.currentEnery += energy;
        }
    }

    public void AbsorbObject(int x, int z, Item who)
    {
        var energy = level.tiles[x, z].DestroyLowest();
        who.currentEnery += energy;
    }
}
